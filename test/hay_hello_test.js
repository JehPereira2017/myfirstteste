import { equal } from 'assert'
import sayHello from '../src/say_hello'

describe('sayHello test',() => {
    it('should return greet with excitment',() => {
        equal(sayHello('sucesso'), 'Hello sucesso!');
    })
})
describe('Array', function() {
    describe('#indexOf()', function() {
      it('should return -1 when the value is not present', function() {
          equal([1, 2, 3].indexOf(4), -1);
      });
    });
  });