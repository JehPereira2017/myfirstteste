FROM node:11-alpine
COPY . .
RUN npm install
RUN npx gulp browserify
EXPOSE 3000
CMD ["npm", "start"]