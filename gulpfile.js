var browserify = require('browserify');
var babelify = require('babelify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');

gulp.task('browserify',function(){
    return browserify('./src/app.js')
        .transform(babelify.configure({
            presets: ["@babel/preset-env"]
        }))
        .bundle()
        .pipe(source('app.js'))
        .pipe(gulp.dest('./public/'));
});
gulp.task('watch', function(){
    gulp.watch('./src/*.js',gulp.series('browserify'))
});
gulp.task('default',gulp.series('watch'));